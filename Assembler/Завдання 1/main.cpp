#include <stdio.h>
#include <conio.h>

extern "C" {
    int weightedsum(int n1, int n2);
    int n1;
}

int main() {
    int n2, res;
    n1 = 1;
    n2 = 2;
    res = weightedsum(n2, 3);
    printf("Res = %d\n", res);

    _getch();
    return 0;
}