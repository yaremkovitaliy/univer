.586
.model flat

extrn _n1: dword
public _weightedsum

.data
	weight dd 5

.code
_weightedsum proc
	push ebp
	mov ebp, esp

	mov eax, [ebp+8]
	mov edx, [ebp+12]
	add eax, edx
	add eax, _n1
	mul weight

	pop ebp
	ret
_weightedsum endp
end