#include <iostream>
using namespace std;

void main()
{
	double z1 = 0, z2 = 0, a, two = 2, three = 3;
	double eight = 8, eleven = 11, four = 4, del = 0, z1_buf;

	cout << "Enter a = ";
	
	while (!(std::cin >> a)) {
		cout << "Input error, re-enter !!!\n a = ";
		std::cin.clear(); 
		while (std::cin.get() != '\n') continue; 
	}
	__asm
	{
		finit
			fld three
			fld eight           //st 1 3  st 0 8
			fdiv  st(1), st(0) //  3/8  st1 = 3/8

			fldpi     //�������� � ���� ����� � st(0) = 3.14
			fmul st(2), st(0) // 3/8 * pi � st(2) �������� ���������

			fld four           // st0 = 4 , st3 = 1.17809
			fld a             // st0 = a, st1= 4 st 4 = 1.17
			fdiv st, st(1)   //st0 = a/4
			fsub st(4), st	//st4 = st4 - st0
			fstp del
			fstp del
			fstp del
			fstp del
			fcos
			fmul st, st
			fstp z1_buf

			fld eleven
			fld eight
			fdivp st(1), st
			fldpi
			fmulp st(1),st
			fld a
			fld four
			fdivp st(1),st
			faddp st(1),st
			fcos
			fmul st,st
			fld z1_buf
			fsub st,st(1)
			fstp z1
			fstp del
		/////////////////// z2
		fld two          // st0 =2
		fsqrt	        //st0 = 1.41
		fld two        //st0=2 st1=1,41
		fdiv st(1),st //st1=0,70
		
					     // st0 =2 , st1=0.70
		fld a			//0=a 1=2 2 =0.70
		fdiv st, st(1) //st0 = a/2
		fsin
		fmul st, st(2)
		fstp z2
	
	}
	cout << "z1 =" << z1<<endl;
	cout << "z2 =" << z2 << endl;
	system("pause");
}


