
/*AH=(BH+Z*MCH)(MD*MEH)*/
public class Monitor {

    int N = 800;
    int p = 4;
    int H = N / p;
    int[] A = new int[N];
    int[][] MC = new int[N][N];
    int[][] MO= new int[N][N];
    int[][] MD = new int[N][N];
    int[] tempVector = new int[N];
    
    /* Shared Resource */
    private int[][] ME = new int[N][N];
    private int[] B = new int[N];
    int a;
    
    /* Protected Values */
    private int F1 = 0;
    private int F2 = 0;

    /* Signal finish input */
    synchronized void signalInput() {
        notify();
        F1++;
    }

    /* Wait signal finish input data */
    synchronized void waitInput() {
        try {
            while (F1 != 4) {
                wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /* Signal finish calculation */
    synchronized void signalCalc() {
        notify();
        F2++;
    }

    /*Wait signal finish calculation*/
    synchronized void waitCalc() {
        try {
            while(F2 != 3) {
                wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    synchronized void writeME(int[][] me) {
        ME = me.clone();
    }
    synchronized int[][] readME(){
        return ME;
    }
    synchronized void write_a(int _a) {
         a=_a;
    }
    synchronized int  read_a(){
        return a;
    }
    synchronized void writeB(int[] b) {
        B = b.clone();
    }
    synchronized int[] readB() {
        return B;
    }
}
