
public class Thread4 extends Thread{
    Monitor mMonitor;

    public Thread4(Monitor monitor) {
        mMonitor = monitor;
    }

    public void run() {
        int size = mMonitor.N;
        int[][] ME = MatrixUtils.inputMatrix(size);
        mMonitor.writeME(ME);
        mMonitor.signalInput(); 
        mMonitor.waitInput();
        
        int a4=mMonitor.read_a();
  
        int[] B4 = mMonitor.readB();
        int[][] ME4 = mMonitor.readME();

        int startPos = 3 * mMonitor.H;
        int endPos = size;

        int[][] tempMatrix = new int[size][size];

        mMonitor.tempVector = MatrixUtils.multiplyVectorByMatrix(mMonitor.tempVector, startPos, endPos, size, B4, mMonitor.MC);
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, mMonitor.MD, ME4);
        tempMatrix=MatrixUtils.subMatrixByInt(tempMatrix, startPos, endPos, size, tempMatrix, a4);  
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, tempMatrix, mMonitor.MO);
        mMonitor.A=MatrixUtils.multiplyTVectorByTMatrix(mMonitor.A, mMonitor.tempVector, tempMatrix, startPos, endPos, size);
        
        mMonitor.signalCalc();

    }
}
