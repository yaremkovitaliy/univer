
public class Main {
    public static void main(String[] args) {
    	int t=4;
        Monitor monitor = new Monitor();
        
        Thread_T1 thread_t1 = new Thread_T1(monitor);
        
        Thread1 threadA = new Thread1(monitor);
        Thread2 threadB = new Thread2(monitor);
        Thread3 threadC = new Thread3(monitor);
        Thread4 threadD = new Thread4(monitor);
        if(t==4) {
        	System.out.println(" T4: ");
	        threadA.start();
	        threadB.start();
	        threadC.start();
	        threadD.start();
        }
        else if(t==1) {
        	System.out.println(" T1: ");
    	    thread_t1.start();
        }

    }
}
