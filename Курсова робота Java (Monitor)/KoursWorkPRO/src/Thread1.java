
public class Thread1 extends Thread {

    private Monitor mMonitor;

    public Thread1(Monitor monitor) {
        mMonitor = monitor;
    }

    public void run() {
        
        int size = mMonitor.N;
        		
        mMonitor.MC = MatrixUtils.inputMatrix(size);
        mMonitor.signalInput();
        mMonitor.waitInput();
        
        int a1= mMonitor.read_a();
        int[] B1 = mMonitor.readB().clone();
        int[][] ME1 = mMonitor.readME().clone();
        int startPos = 0;
        int endPos = mMonitor.H;
        int[][] tempMatrix = new int[size][size];

        mMonitor.tempVector = MatrixUtils.multiplyVectorByMatrix(mMonitor.tempVector, startPos, endPos, size, B1, mMonitor.MC);
     //   MatrixUtils.outputVector(mMonitor.tempVector);
      //  System.out.println();
     //   System.out.print("MD : ");
     //   MatrixUtils.outputMatrix(mMonitor.MD, size);
     //   System.out.print("ME1 : ");
    //    MatrixUtils.outputMatrix(ME1, size); 
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, mMonitor.MD, ME1);
      //  System.out.print("Temp1 : ");
       // MatrixUtils.outputMatrix(tempMatrix, size);
        tempMatrix=MatrixUtils.subMatrixByInt(tempMatrix, startPos, endPos, size, tempMatrix, a1);//�� �� ���� ��� ���������� 
      //  System.out.print("Temp2 : ");
       // MatrixUtils.outputMatrix(tempMatrix, size);
      //  System.out.print("MO : ");
      //  MatrixUtils.outputMatrix(mMonitor.MO, size);
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, tempMatrix, mMonitor.MO);
     //   System.out.print("Temp3 : ");
       // MatrixUtils.outputMatrix(tempMatrix, size);
        mMonitor.A=MatrixUtils.multiplyTVectorByTMatrix(mMonitor.A, mMonitor.tempVector, tempMatrix, startPos, endPos, size);
       // MatrixUtils.outputMatrix(tempMatrix, size);
        mMonitor.signalCalc();
    }
}
