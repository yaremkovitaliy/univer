import java.util.Date;

public class Thread_T1 extends Thread{

    private Monitor mMonitor;

    public Thread_T1(Monitor monitor) {
        mMonitor = monitor;
    }

    public void run() {
    	
    	Date TimeStart = new Date();
    	int N=800;
        int size = N;
        
        int[] A= new int[N];
        int[] tempVector=new int[N];
        int[] B = MatrixUtils.inputVector(size);
        int a=1;
        int[][] ME = MatrixUtils.inputMatrix(size);
        int[][] MO = MatrixUtils.inputMatrix(size);
        int[][] MD = MatrixUtils.inputMatrix(size);
        int[][] MC = MatrixUtils.inputMatrix(size);
        
        int startPos = 0;
        int endPos = N;
        int[][] tempMatrix = new int[size][size];

        mMonitor.tempVector = MatrixUtils.multiplyVectorByMatrix(tempVector, startPos, endPos, size, B, MC);
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, MD, ME);
        tempMatrix=MatrixUtils.subMatrixByInt(tempMatrix, startPos, endPos, size, tempMatrix, a); 
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, tempMatrix, MO);
        A=MatrixUtils.multiplyTVectorByTMatrix(A,tempVector, tempMatrix, startPos, endPos, size);
        
     
        Date TimeFinish = new Date();
        double Time = ((double)TimeFinish.getTime() - (double)TimeStart.getTime())/1000;

        System.out.println("Calculation time: " + Time);

        MatrixUtils.outputVector(A);

    }
}
