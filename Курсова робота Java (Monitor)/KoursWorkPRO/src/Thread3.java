import java.util.Date;

public class Thread3 extends Thread{

    private Monitor mMonitor;

    public Thread3(Monitor monitor) {
        mMonitor = monitor;
    }

    public void run() {
    	
    	
    	
        int size = mMonitor.N;
        
        int[] B = MatrixUtils.inputVector(size);
        mMonitor.writeB(B);
        int a=1;
        mMonitor.write_a(a);
        
        mMonitor.signalInput();
        mMonitor.waitInput();
        
        int a3=mMonitor.read_a();
   
        int[] B3 = mMonitor.readB();
        int[][] ME3 = mMonitor.readME();

        int startPos = 2 * mMonitor.H;
        int endPos = 3 * mMonitor.H;
        int[][] tempMatrix = new int[size][size];

        Date TimeStart = new Date();
        mMonitor.tempVector = MatrixUtils.multiplyVectorByMatrix(mMonitor.tempVector, startPos, endPos, size, B3, mMonitor.MC);
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, mMonitor.MD, ME3);
        tempMatrix=MatrixUtils.subMatrixByInt(tempMatrix, startPos, endPos, size, tempMatrix, a3);
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, tempMatrix, mMonitor.MO);
        mMonitor.A=MatrixUtils.multiplyTVectorByTMatrix(mMonitor.A, mMonitor.tempVector, tempMatrix, startPos, endPos, size);
        
        
        mMonitor.waitCalc();

        Date TimeFinish = new Date();
        double Time = ((double)TimeFinish.getTime() - (double)TimeStart.getTime())/1000;

        System.out.println("Calculation time: " + Time);

        MatrixUtils.outputVector(mMonitor.A);

    }
}
