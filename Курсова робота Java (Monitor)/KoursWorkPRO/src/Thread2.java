
public class Thread2 extends Thread{

    Monitor mMonitor;

    public Thread2(Monitor monitor) {
        mMonitor = monitor;
    }

    public void run() {

        int size = mMonitor.N;

        mMonitor.MD = MatrixUtils.inputMatrix(size);
        mMonitor.MO = MatrixUtils.inputMatrix(size);
        mMonitor.signalInput();
        mMonitor.waitInput();

        int a2=mMonitor.read_a();
      
        int[] B2 = mMonitor.readB();
        int[][] ME2 = mMonitor.readME();

        int startPos = mMonitor.H;
        int endPos = 2 * mMonitor.H;

        int[][] tempMatrix = new int[size][size];

        mMonitor.tempVector = MatrixUtils.multiplyVectorByMatrix(mMonitor.tempVector, startPos, endPos, size, B2, mMonitor.MC);
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, mMonitor.MD, ME2);
        tempMatrix=MatrixUtils.subMatrixByInt(tempMatrix, startPos, endPos, size, tempMatrix, a2); 
        tempMatrix=MatrixUtils.addMatrix(tempMatrix, startPos, endPos, size, tempMatrix, mMonitor.MO);
        mMonitor.A=MatrixUtils.multiplyTVectorByTMatrix(mMonitor.A, mMonitor.tempVector, tempMatrix, startPos, endPos, size);
        

        mMonitor.signalCalc();
    }
}
