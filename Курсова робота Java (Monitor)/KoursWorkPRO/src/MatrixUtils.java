public  class MatrixUtils {

    private MatrixUtils() {}
    //������� �������
    public static int[][] multiplyMatrix(int[][] resMatrix, int startPos, int endPos, int size, int[][] matrixShareRes, int[][] matrix2){
        for (int i = startPos; i < endPos; i++) {
            for (int j = 0; j < size; j++) {
                for (int k = 0; k < size; k++) {
                    resMatrix[i][j] += matrixShareRes[j][k] * matrix2[i][k];
                }
            }
        }
        return resMatrix;
    }
    //���� �������
    public static int[][] addMatrix(int[][] resMatrix, int startPos, int endPos, int size, int[][] matrixShareRes, int[][] matrix2){
        for (int i = startPos; i < endPos; i++) {
            for (int j = 0; j < size; j++) {
                    resMatrix[i][j] = matrixShareRes[i][j] + matrix2[i][j];
            }
        }
        return resMatrix;
    }
    //������� ������ �����
    public static int[][]  subMatrixByInt(int[][] resMatrix, int startPos, int endPos, int size, int[][] matrixShareRes, int a){
        for (int i = startPos; i < endPos; i++) {
            for (int j = 0; j < size; j++) {
                    resMatrix[i][j] -= matrixShareRes[i][j] - a;
            }
        }
        return resMatrix;
    }
    
    //�������� Vector*Matrix
    public static int[] multiplyVectorByMatrix(int[] resVector, int startPos, int endPos, int size, int[] vectShare, int[][] matrix){
        for (int i = startPos; i < endPos; i++) {
            for (int j = 0; j < size; j++) {
                resVector[i] = resVector[i] + vectShare[j] * matrix[i][j];
            }
        }
        return resVector;
    }
    // ������ �� �������
    public static int[] multiplyTVectorByTMatrix(int[] resVector, int[] vectShare, int[][] matrix, int startPos, int endPos, int size){
        for (int i = startPos; i < endPos; i++) {
            for (int j = 0; j < size; j++) {
                resVector[i] = resVector[i] +  vectShare[i] * matrix[i][j];
            }
        }
        return resVector;
    }
    // �������� �������
    public static int[][] inputMatrix(int size) {
        int[][] input = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                input[i][j] = 1;
            }
        }
        return input;
    }
    //�������� �������
    public static int[] inputVector(int size) {
        int[] input = new int[size];
        for (int i = 0; i < size; i++) {
            input[i] = 1;
        }
        return input;
    }
    
    public static void outputMatrix(int[][] output,int size) {
    	 for (int i = 0; i < size; i++) {
    		 System.out.println();
             for (int j = 0; j < size; j++) {
            	 System.out.print("    ");
                System.out.print(output[i][j]);
             }
         }
    	 System.out.println();
    }
    
    
    public static void outputVector(int[] output) {
        for (int i : output) {
            System.out.print(i + " ");
        }
    }
    // ���� �������
    public static int[] sumOfVectors(int[] tempVector, int[] b, int startPos, int endPos) {
        for (int i = startPos; i < endPos; i++) {
        	 System.out.println("       ");
            tempVector[i] = tempVector[i] + b[i];
        }
        System.out.println();
        return tempVector;
        
    }
}
